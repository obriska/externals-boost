# External Boost Libraries

This is not a standalone CMake project. Typically it is used as a Git submodule and included into a parent CMake project.

## Prerequisites

- CMake (>= 3.5)
- GCC (>= 5.4)
- GNU Make (>= 4.1)
- Git (>= 2.7)

## How To Use This Project

In the root CMake project add a Git submodule:

```
$ mkdir -p externals
$ git submodule add -- https://github.com/obriska/externals-boost.git externals/boost
```

In the root CMakeLists.txt file add the following lines:

```cmake
# Use EXTERNALS_DIR to refer to the directory where external projects are residing
set(EXTERNALS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/externals")

# Configuring Boost libraries
set(Boost_VERSION 1.65.1)
set(Boost_LIBS
        assert
        exception
        system
        )

# Add the external Boost libraries
add_subdirectory("${EXTERNALS_DIR}/boost")

# Configure system include directories
include_directories(SYSTEM "${Boost_INCLUDE_DIR}")
```

Link each executable and shared library with Boost libraries:

```cmake
target_link_libraries(${PROJECT_NAME} boost_system)
```

## License

MIT License

Copyright (c) 2018 Olegs Briska.

See LICENSE file for details.
